#!/usr/bin/python
# -*- coding: utf-8 -*-
import io, threading
from FilterFramework import FilterFramework as ff

class FilterTemplate(ff):
    def run():
        databyte
        while (true):
            '''   
            El programa puede insertar codigo para las operaciones de filtrado aqui.
            Notese que los datos deben ser recibidos y enviados un byte a la vez.
            Esto se ha hecho de esta forma para adherir al paradigma Pipe and Filter
            y proveer un alto grado de portabilidad entre filtros. Sin embargo, tu
            mismo debes reconstruir los datos. Primero leemos un byte del flujo de entrada... 
            '''
            try:
                databyte = readFilterInputPort();

                #Aqui podriamos insertar codigo que opere en el flujo de entrada...
		#Posteriormente escribiemos un byte al puerto de salida

                writeFilterOutputPort(databyte);
                
                '''
                Cuando se llega al final del flujo de entrada se envia una
		excepcion que es mostrada abajo. En este punto se debe
		terminar todo procesamiento, cerrar los puertos y salir.
		'''
            except EndOfStreamException as e:
                closePorts();
		break;
