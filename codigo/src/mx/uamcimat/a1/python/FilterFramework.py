#!/usr/bin/python
# -*- coding: utf-8 -*-
import io, threading

'''
/******************************************************************************************************************
* Archivo:FilterFramework.py
* Curso: 17655
* Proyecto: Tarea 1
* Copyright: Copyright (c) 2003 Carnegie Mellon University
* Versions:
*	1.0 November 2008 - Initial rewrite of original assignment 1 (ajl).
*
* Descripcion:
*
* Esta clase de base define el esqueleto de un framework de filtros que define un filtro en termino de 
* puertos de entrada y salida. Todos los filtros deben ser definidos en base a este framework, esto es que
* los filtros deben extender esta clase para ser considerados como filtros validos del sistema. Los filtros
* se ejecutan en hilos individuales hasta que el puerto de entrada (inputport) ya no tiene datos - en ese
* momento el filtro termina cualquier trabajo pendiente y termina.
* 
* 
*
* Atributos:
*
* InputReadPort:	Este es el puerto de entrada del filtro. Este puerto se conecta al puerto de salida de otro filtro.
* 					Todos los filtros se conectan a otros filtros mediante conexiones de los puertos de entrada a los
* 					puertos de salida de los otros filtros. Esto es manejado por el metodo Connect()
*
* OutputWritePort:	Este es el puerto de salida del filtro. En esencia, el trabajo del filtro es de leer data del puerto
* 					de entrada, realizar alguna operacion sobre los datos, y escribir los datos transformados al puerto
* 					de salida.	
* 
* InputFilter:		Esta es una referencia al filtro que esta conectado al puerto de entrada. Esta referencia sirve para
* 					determinar cuando termina de enviar datos el filtro conectado al puerto de entrada.
*
* Metodos:
*
*	public void Connect( FilterFramework Filter )
*	protected byte ReadFilterInputPort()
*	protected void WriteFilterOutputPort(byte datum)
*	protected boolean EndOfInputStream()
*
******************************************************************************************************************/
'''

class FilterFramework(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

        #Puertso de entrada y salida del filtro
        inputReadPort = None
	outputWritePort = None
        

	#La referencia siguiente a un filtro es usada por que los pipes de java permiten
	#detectar de manera confiable cuando termina el flujo el puerto de entrada del filtro.
	#Esta referencia apunta al filtro previo en la red y cuando muere sabemos que ha
	#terminado el envio de datos.

        inputFilter
    #Excepción del flujo de datos
    class EndOfStreamException(Exception):
        def __init__(self, message, Errors):
            Exception.__init__(self, message)
                
    '''
    * Este metodo conecta a los filtros entre ellos. Todas las conexiones
    * se hacen a traves del puerto de entrada de cada filtro, esto es que el inputport
    * es conectado al puerto de salida de otro filtro a traves de este metodo.
    * 
    * @param previousFilter
    * @return void
    * @exception IOException
    '''
    def connect(previousFilter):
        try:
            inputReadPort.connect(previousFilter.outputWritePort)
            inputFilter = previousFilter
        except Exception as ex:
            print "\n" + self.getName() + " FilterFramework error connecting::"+ ex
    
    '''
    /***************************************************************************
    * CONCRETE METHOD:: ReadFilterInputPort
    * Purpose: This method reads data from the input port one byte at a time.
    *
    * Arguments: void
    *
    * Returns: byte of data read from the input port of the filter.
    *
    * Exceptions: IOExecption, EndOfStreamException (rethrown)
    *
    ****************************************************************************/

    * Este metodo lee datos del puerto de entrada un byte a la vez
    * 
    * @return
    * @throws EndOfStreamException
    '''
    def readFilterInputPort():
        '''
        que puede haber retrasos en filtros previos, primero esperamos
        que no haya datos disponibles en el puerto de entrada. Checamos,
        no hay datos disponibles en el puerto de entrada, esperamos por un
        de segundo y checamos de nuevo. Notese que no hay un timeout y
        el filtro previo esta bloqueado, esto puede resultar en esperas 
        dentro de este ciclo. Es necesario checar si estamos al final
        un flujo de datos en el ciclo de espera por que es posible que el filtro
        complete mientras estamos esperando. Si esto ocurre y no verificamos
        sea el final de un flujo de datos, podriamos esperar para siempre
        un filtro previo que ya termino anteriormente. Por desgracia, los
        de java no envian excepciones cuando la tuberia de entrada se rompe. 
        que hacemos aqui es ver si el filtro previo esta vivo. Si lo,
        asumimos que la tuberia sigue sigue abierta y se estan enviando datos.
        el filtro no esta vivo, entonces asumimos que el fin del flujo ha
        alcanzado.
        '''
        datum = buffers(0)
	try:

            while inputReadPort.available() == 0:

                if (endOfInputStream()):
                    EndOfStreamException("End of input stream reached")

		sleep(250);

	except EndOfStreamException as Error:
            raise Error;
        except Exception as Error:
            print "\n" + this.getName() + " Error in read port wait loop::" + Error 
		 #Si, finalmente, un byte de datos esta disponible en la tuberia de entrada
		 #podemos leerlo. Leemos y escribimos un byte de los puertos
        try:
            datum = inputReadPort.read();
            return datum;
        except Exception as Error:
            print "\n" + this.getName() + " Pipe read error::" + Error
            return datum;

	'''
	Este metodo escribe datos al puerto de salida un byte a la vez
	
	@param datum el byte que sera escrito
	'''

	def writeFilterOutputPort(datum):
            try:
                outputWritePort.write(datum)
                outputWritePort.flush();
            except Exception as Error:
                print "\n" + this.getName() + " Pipe write error::" + Error

            return;

	'''
	Este metodo es usado de forma interna al framework por lo cual es privdo. Regresa
	un valor verdadero cuando no hay mas datos a leer en el puerto de entrada del filtro.
	Lo que hace en realidad es cecar si el filtro previo sigue vivo. Esto se hace pues java
	no maneja de manera confiable las tuberias de entrada y frecuentemente sigue leyendo
	(basura) de una tuberia de entrada rota
	
	@return true si el filtro previo ha dejado de enviar datos false de lo contrario
        '''

	def endOfInputStream():
            return True if inputFilter.isAlive() else False

	'''
        /***************************************************************************
	* CONCRETE METHOD:: ClosePorts
	* Purpose: This method is used to close the input and output ports of the
	* filter. It is important that filters close their ports before the filter
	* thread exits.
	*
	* Arguments: void
	*
	* Returns: void
	*
	* Exceptions: IOExecption
	*
	****************************************************************************/
	
	Este metodo es usado para cerrar los puertos de entrada y salida del filtro.
	Es importante que los filtros cierren sus puertos antes de que el hilo
	del filtro salga.
        '''

	def  closePorts():
            try:
                inputReadPort.close();
		outputWritePort.close();
            except Exception as Error:
                print "\n" + this.getName() + " ClosePorts error::" + Error

	'''
        Este es un metodo abstracto definido por Thread. Es llamado cuando el hilo
	arranca mediante una llamada al metodo Thread.start(). En este caso, el metodo
	run() debe ser sobreescrito por el programador de filtros
        '''

	def run():
            pass
